set(Boost_USE_STATIC_LIBS OFF)

find_package(Boost REQUIRED COMPONENTS unit_test_framework)
include_directories(${Boost_INCLUDE_DIRS})

add_executable(test_binary_heap binary_heap_test.cpp)
target_link_libraries(test_binary_heap ${Boost_LIBRARIES})
target_link_libraries(test_binary_heap ds)

add_test(BinaryHeap test_binary_heap)
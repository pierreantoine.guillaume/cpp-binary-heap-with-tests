#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "ds/tree/binary_heap.h"

BOOST_AUTO_TEST_SUITE(BinaryHeapSuite)

std::string view(const binary_heap &heap)
{
  std::string s;

  for (const auto &i : heap)
  {
    s += std::to_string(i) + ',';
  }
  return s.substr(0, s.size() - 1);
}

BOOST_AUTO_TEST_CASE(Fill)
{
  auto heap = binary_heap();

  for (const auto &i : {10, 11, 3, 6, 4, 2})
  {
    heap.add(i);
  }

  BOOST_REQUIRE_EQUAL(view(heap), "2,4,3,11,6,10");
}

BOOST_AUTO_TEST_CASE(Sort)
{
  binary_heap heap;
  for (const auto &i : {10, 11, 3, 6, 4, 2})
  {
    heap.add(i);
  }
  std::string s;
  while (!heap.empty())
  {
    s += std::to_string(heap.poll()) + ',';
    heap.pop();
  }

  BOOST_REQUIRE_EQUAL(s.substr(0, s.size() - 1), "2,3,4,6,10,11");
}

BOOST_AUTO_TEST_SUITE_END()
#include "binary_heap.h"

binary_heap::ram_interface::size_type parent(binary_heap::ram_interface::size_type index)
{
  return (index - 1) / 2;
}

binary_heap::ram_interface::size_type left_child(binary_heap::ram_interface::size_type index)
{
  return index * 2 + 1;
}

binary_heap::ram_interface::size_type right_child(binary_heap::ram_interface::size_type index)
{
  return left_child(index) + 1;
}

void binary_heap::add(stored_type value)
{
  tree.emplace_back(value);
  top_down_sort();
}

binary_heap::stored_type binary_heap::poll() const
{
  return tree[0];
}

binary_heap::ram_interface::size_type binary_heap::size() const
{
  return tree.size();
}

inline bool binary_heap::compareValues(binary_heap::ram_interface::size_type first, binary_heap::ram_interface::size_type second) const
{
  return tree[first] < tree[second];
}

inline binary_heap::ram_interface::size_type binary_heap::smallest_child(binary_heap::ram_interface::size_type index) const
{
  auto left = left_child(index);
  auto right = right_child(index);
  if (left >= size())
  {
    return index;
  }
  if (right >= size())
  {
    return compareValues(index, left) ? index : left;
  }
  if (compareValues(left, right))
  {
    return compareValues(index, left) ? index : left;
  }
  return compareValues(index, right) ? index : right;
}

inline void binary_heap::top_down_sort()
{
  if (size() < 2)
  {
    return;
  }
  auto index = size() - 1;
  while (index > 1)
  {
    auto ref = parent(index);
    if (compareValues(index, ref))
    {
      swap(index, ref);
      index = ref;
      continue;
    }
    break;
  }
}

inline void binary_heap::bottom_up_sort()
{
  if (size() < 2)
  {
    return;
  }
  binary_heap::ram_interface::size_type index = 0;
  while (index < size())
  {
    auto smallest = smallest_child(index);
    if (smallest == index)
    {
      return;
    }
    swap(index, smallest);
    index = smallest;
  }
}

bool binary_heap::empty() const
{
  return size() == 0;
}

void binary_heap::pop()
{
  switch (size())
  {
    case 1:tree.pop_back();
    case 0:return;
  }
  swap(0, size() - 1);
  tree.pop_back();
  bottom_up_sort();
}

inline void binary_heap::swap(const binary_heap::ram_interface::size_type first, const binary_heap::ram_interface::size_type second)
{
  using std::swap;
  swap(tree[first], tree[second]);
}

binary_heap::ram_interface::const_iterator binary_heap::begin() const
{
  return tree.begin();
}

binary_heap::ram_interface::const_iterator binary_heap::end() const
{
  return tree.end();
}

#pragma once

#include <vector>


class binary_heap
{
 public:
  typedef unsigned stored_type;
  typedef std::vector<stored_type> ram_interface;
 private:
  ram_interface tree;

  void top_down_sort();
  void bottom_up_sort();

  void swap(ram_interface::size_type, ram_interface::size_type);
  [[nodiscard]] bool compareValues(ram_interface::size_type first, ram_interface::size_type second) const;
  [[nodiscard]] ram_interface::size_type smallest_child(ram_interface::size_type index) const;
  [[nodiscard]] ram_interface::size_type size() const;
 public:
  binary_heap() = default;
  void add(stored_type value);
  [[nodiscard]] stored_type poll() const;
  [[nodiscard]] bool empty() const;

  void pop();

  [[nodiscard]] ram_interface::const_iterator begin() const;
  [[nodiscard]] ram_interface::const_iterator end() const;
};


